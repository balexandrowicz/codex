<?php get_header(); ?>

	<div class="hero">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<h1>Projects</h1>
				</div>
			</div>
		</div>
	</div>

	<div class="container default">
		<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
			<div class="panel panel-default project">
				<div class="panel-heading">
					<h1><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h1>
				</div>
				<div class="panel-body">
					<div class="row">
						<div class="col-md-12">
							<div class="row">
								<div class="col-md-2 project-thumbnail">
									<a href="<?php the_permalink(); ?>"><?php the_post_thumbnail(); ?></a>
								</div>
								<div class="col-md-10">
									<div class="row first">
										<div class="col-md-6">
											<h2>Project Date</h2>
											<?php the_field('project-date'); ?>
										</div>
										<div class="col-md-6">
											<h2>Client Contacts</h2>
											<?php the_field('client-contact'); ?>
										</div>
									</div>
									<div class="row second">
										<div class="col-md-6">
											<h2>Project Activities</h2>
											<?php the_field('project-activities'); ?>
										</div>
										<div class="col-md-6">
											<h2>Other Vendors</h2>
											<?php the_field('other-vendors'); ?>
										</div>
									</div>
								</div>
							</div>
						</div><!--/.col-md-12-->
					</div><!--/.row-->
				</div><!--/.panel-body-->
			</div><!--/.panel-->
			<?php endwhile; else : ?>
				<p><?php _e( 'Sorry, no posts matched your criteria.' ); ?></p>
			<?php endif; ?>
	</div> <!--/.container-->

<?php get_footer(); ?>