<?php get_header(); ?>

	<div class="container default">
		<div class="row">
			<div class="col-md-8">
				<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
					<h1 class="project-title"><?php the_title(); ?></h1>
					<?php the_content(); ?>
				<?php endwhile; else : ?>
					<p><?php _e( 'Sorry, no posts matched your criteria.' ); ?></p>
				<?php endif; ?>
			</div><!--/.col-md-8-->
			<div class="col-md-4">
				<div class="panel panel-default project-info-secondary">
					<div class="panel-body">
						<h2>Method</h2>
						<p><?php the_field('project-method'); ?></p>
						<hr>
						<h2>Participants</h2>
						<p><?php the_field('project-participants'); ?></p>
					</div>
				</div>
			</div><!--/.col-md-4-->
		</div><!--/.row-->
		<div class="row downloads">
			<div class="col-md-12">
				<?php if( get_field('executive-summary') ): ?>
					<a href="<?php the_field('executive-summary'); ?>" class="btn btn-primary">Executive Summary</a>
				<?php endif;?>
				<?php if( get_field('presentation') ): ?>
					<a href="<?php the_field('presentation'); ?>" class="btn btn-primary">Presentation</a>
				<?php endif;?>
			</div><!--/.col-md-12-->
		</div><!--/.row-->
		<div class="row videos">
			<div class="col-md-12">
				<h2>Videos</h2>
				<?php the_field('project-videos'); ?>
			</div>
		</div>
	</div><!--/.container-->

<?php get_footer(); ?>